(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */


    // Open WP media selector
    $(document).on('click', '.js-select-media', (e) => {
        e.preventDefault();
        const target_id = $(e.target).data('target');
        const input_element = $('#' + target_id);
        const img_element = $('#' + target_id + '-img');
        const image = wp.media({
            title: 'Select An Image',
            multiple: false,
            library: { 
                type: 'image'
            },
            button: {
                text: 'Select'
            },
        }).open().on('select', (e) => {
            const image_url = image.state().get('selection').first().toJSON().url;
            input_element.val(image_url).trigger('change');
            img_element.attr('src', image_url);
        });
    });

    // Remove selected Image
    $(document).on('click', '.js-remove-media', (e) => {
        e.preventDefault();
        const element = $(e.target);
        const target_id = element.data('target');
        $('#' + target_id).val('').trigger('change');
        $('#' + target_id + '-img').attr('src', '');
    });

})( jQuery );


// var image_field;
// jQuery(function($){
//   $(document).on('click', 'input.select-img', function(evt){
//     image_field = $(this).siblings('.img');
//     tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
//     return false;
//   });
//   window.send_to_editor = function(html) {
//     imgurl = $('img', html).attr('src');
//     image_field.val(imgurl);
//     tb_remove();
//   }
// });


