<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              alchemizt.org
 * @since             1.0.0
 * @package           Mai_Theme_Addons
 *
 * @wordpress-plugin
 * Plugin Name:       Mai Theme Addons
 * Plugin URI:        alchemizt.org
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Dominic Fagan
 * Author URI:        alchemizt.org
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mai-theme-addons
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'Mai_Theme_Addons_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-mai-theme-addons-activator.php
 */
function activate_Mai_Theme_Addons() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-mai-theme-addons-activator.php';
	Mai_Theme_Addons_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-mai-theme-addons-deactivator.php
 */
function deactivate_Mai_Theme_Addons() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-mai-theme-addons-deactivator.php';
	Mai_Theme_Addons_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_Mai_Theme_Addons' );
register_deactivation_hook( __FILE__, 'deactivate_Mai_Theme_Addons' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-mai-theme-addons.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_Mai_Theme_Addons() {

	$plugin = new Mai_Theme_Addons();
	$plugin->run();

}
run_Mai_Theme_Addons();
