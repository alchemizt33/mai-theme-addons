<?php

/**
 * Fired during plugin activation
 *
 * @link       alchemizt.org
 * @since      1.0.0
 *
 * @package    Mai_Theme_Addons
 * @subpackage Mai_Theme_Addons/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Mai_Theme_Addons
 * @subpackage Mai_Theme_Addons/includes
 * @author     Dominic Fagan <alchemizt33@gmail.com>
 */
class Mai_Theme_Addons_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {


	    /**
	     * This only required if custom post type has rewrite!
	     *
	     * Remove rewrite rules and then recreate rewrite rules.
	     *
	     * This function is useful when used with custom post types as it allows for automatic flushing of the WordPress
	     * rewrite rules (usually needs to be done manually for new custom post types).
	     * However, this is an expensive operation so it should only be used when absolutely necessary.
	     * See Usage section for more details.
	     *
	     * Flushing the rewrite rules is an expensive operation, there are tutorials and examples that suggest
	     * executing it on the 'init' hook. This is bad practice. It should be executed either
	     * on the 'shutdown' hook, or on plugin/theme (de)activation.
	     *
	     * @link https://codex.wordpress.org/Function_Reference/flush_rewrite_rules
	     */
	    flush_rewrite_rules();

	}

}
