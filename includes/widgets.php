<?php
// Creating the widget 
class wpb_widget extends WP_Widget {
  
	function __construct() {

		parent::__construct(
		  
			// Base ID of your widget
			'wpb_widget', 
			  
			// Widget name will appear in UI
			__('WPBeginner Widget', 'wpb_widget_domain'), 
			  
			// Widget description
			array( 'description' => __( 'Sample widget based on WPBeginner Tutorial', 'wpb_widget_domain' ), ) 
		);
	}
	  
	// Creating widget front-end
	  
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
	    $image_id   = attachment_url_to_postid( $instance['image_url'] ); 
	    $image_url = wp_get_attachment_image_src( $image_id, 'medium' )[0];
		  
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];
		  

	    // basic output just for this example
	    echo '
		      <a href="' . home_url() . '">
		        <img src="'.esc_url($image_url).'" />
		      </a>
		      <p style="padding: 7px">"<i>Our wounds are the openings in which the light enters. The light is the medicine which not only heals our wounds, but illuminates our entire being.</i>"</p> 
		      <p style="padding: 3px">Contact Us: <b>+51 927 370 320</b></p>
		      <p style="padding: 3px">Email Us: <a href="mailto:support@mainiti.co">support@mainiti.co</a></p>
		';


		echo $args['after_widget'];
	}
	          
public function form( $instance ) {
    $title       = ! empty( $instance['title'] ) ? $instance['title'] : '';
    $url         = ! empty( $instance['url'] ) ? $instance['url'] : '';
    $image_url         = ! empty( $instance['image_url'] ) ? $instance['image_url'] : '';

    $image_id   = attachment_url_to_postid( $image_url ); 
    $thumbnail_url = wp_get_attachment_image_src( $image_id, 'medium' )[0];
    ?>
    <p>
        <label
                for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
            Title:
        </label>
        <input
                class="widefat"
                id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
                name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
                type="text"
                value="<?php echo esc_attr( $title ); ?>">
    </p>
    <p>
        <label
                for="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>">
            URL:
        </label>
        <input
                class="widefat"
                id="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>"
                name="<?php echo esc_attr( $this->get_field_name( 'url' ) ); ?>"
                type="text"
                value="<?php echo esc_attr( $url ); ?>"
                dir="ltr">
    </p>
    <p>
        <img
             class="image-fluid"
             id="<?php echo esc_attr( $this->get_field_id( 'image_url' ) ) . "-img"; ?>"
             src="<?php echo esc_url( $thumbnail_url ); ?>"
             alt="" />
        <input type="text"
               id="<?php echo esc_attr( $this->get_field_id( 'image_url' ) ); ?>"
               name="<?php echo esc_attr( $this->get_field_name( 'image_url' ) ); ?>"
               value="<?php echo esc_url( $image_url ); ?>" />
        <br>
        <a href="#" class="button js-select-media"
           data-target="<?php echo esc_attr( $this->get_field_id( 'image_url' ) ); ?>">
            Select Image
        </a>
        <a href="#" class="button js-remove-media"
           data-target="<?php echo esc_attr( $this->get_field_id( 'image_url' ) ); ?>">
            Remove Image
        </a>
    </p>
    <?php
}

public function update( $new_instance, $old_instance ) {
    $instance                = array();
    $instance['title']       = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['url']         = ( ! empty( $new_instance['url'] ) ) ? strip_tags( $new_instance['url'] ) : '';
    $instance['image_url']         = ( ! empty( $new_instance['image_url'] ) ) ? strip_tags( $new_instance['image_url'] ) : '';

    $image_url = $instance['image_url'];
    $image_id   = attachment_url_to_postid( $image_url ); 

    return $instance;
}

	// Class wpb_widget ends here
} 
 
// queue up the necessary js
function hrw_enqueue()
{
  wp_enqueue_style('thickbox');
  wp_enqueue_script('media-upload');
  wp_enqueue_script('thickbox');
  // moved the js to an external file, you may want to change the path
  wp_enqueue_script('hrw', '/wp-content/plugins/mai-theme-addons/admin/js/widgets.js', null, null, true);
}
 
add_action('admin_enqueue_scripts', 'hrw_enqueue');

 
// Register and load the widget
function wpb_load_widget() {
    register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );