<?php
add_action( 'wp_head', 'mai_header_fade_css' );
function mai_header_fade_css() {

	// Bail if in the Dasbhoard.
	if ( is_admin() ) {
		return;
	}

	/**
	 * Fade onScroll.
	 */
	echo "<style>

	:root {
		--header-background: 1;
	}

    .site-header { 
    	opacity: var(--header-opacity);
    	transition: opacity .8s ease-in-out;
   		-moz-transition: opacity .8s ease-in-out;
   		-webkit-transition: opacity .8s ease-in-out; 
    }

    .site-header {
    	background-color: rgba(255,255,255,var(--header-background)); 
    }

    header:hover { opacity: 1; }

	</style>";
}



/**
 * Add body class to enabled specific settings.
 *
 * @since   Unknown.
 *
 * @param   array  $classes  The body classes.
 *
 * @return  array  $classes  The modified classes.
 */

add_filter( 'body_class', 'mai_do_settings_custom_body_classes' );
function mai_do_settings_custom_body_classes( $classes ) {

	// Header style.
	$header_style = genesis_get_option( 'header_style' );
	if ( $header_style && ! is_page_template( 'landing.php' ) ) {
		switch ( $header_style ) {
			case 'sticky_fade':
				$classes[] = 'has-sticky-header';
				$classes[] = 'has-fade-header';
			break;
			case 'sticky_transparent':
				$classes[] = 'has-sticky-header';
				$classes[] = 'has-transparent-header';
			break;
		}
	}

	return $classes;

}




/**
 * Add custom options to the customizer menu..
 */
add_action( 'customize_register', 'custom_mai_register_customizer_header_footer_settings',33 );
function custom_mai_register_customizer_header_footer_settings( $wp_customize ) {


	/* ************ *
	 * Mai Settings *
	 * ************ */


	$section        = 'mai_header_footer';
	$settings_field = 'genesis-settings';

	// Header style.

	$wp_customize->add_control(
		'header_style',
		array(
			'label'    => __( 'Header style', 'mai-theme-engine' ),
			'section'  => $section,
			'settings' => _mai_customizer_get_field_name( $settings_field, 'header_style' ),
			'type'     => 'select',
			'priority' => 1,
			'choices'  => array(
				'standard'      => __( 'Standard Header' ),
				'sticky'        => __( 'Sticky Header' ),
				'reveal'        => __( 'Reveal Header' ),
				'sticky_shrink' => __( 'Sticky/Shrink Header' ),
				'reveal_shrink' => __( 'Reveal/Shrink Header' ),
				'sticky_fade' => __( 'Sticky/Fade Header' ),
				'sticky_transparent' => __( 'Sticky/Transparent Header' ),
			),
		)
	);


}
