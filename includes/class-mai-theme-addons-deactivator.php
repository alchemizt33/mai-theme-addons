<?php

/**
 * Fired during plugin deactivation
 *
 * @link       alchemizt.org
 * @since      1.0.0
 *
 * @package    Mai_Theme_Addons
 * @subpackage Mai_Theme_Addons/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Mai_Theme_Addons
 * @subpackage Mai_Theme_Addons/includes
 * @author     Dominic Fagan <alchemizt33@gmail.com>
 */
class Mai_Theme_Addons_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	    /**
	     * This only required if custom post type has rewrite!
	     */
	    flush_rewrite_rules();

	}

}
