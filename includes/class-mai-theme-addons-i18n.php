<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       alchemizt.org
 * @since      1.0.0
 *
 * @package    Mai_Theme_Addons
 * @subpackage Mai_Theme_Addons/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Mai_Theme_Addons
 * @subpackage Mai_Theme_Addons/includes
 * @author     Dominic Fagan <alchemizt33@gmail.com>
 */
class Mai_Theme_Addons_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'mai-theme-addons',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
