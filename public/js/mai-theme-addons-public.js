(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

})( jQuery );

document.addEventListener( 'DOMContentLoaded', function() {

	var	body               = document.querySelector( 'body' ),
		header             = document.querySelector( '.site-header' ),
        hasFadeHeader    = body.classList.contains( 'has-fade-header' ),
        hasTransparentHeader    = body.classList.contains( 'has-transparent-header' )

	// Make sure we have a header.
	if ( header ) {

		var customScroll = basicScroll.create({
			elem: document.querySelector( '#header-trigger' ),
			from: 'top-top',
			to: 'bottom-top',
			props: hasFadeHeader ? {
				'--header-opacity': {
					from: '1',
					to: '0.25',
				},
			} : hasTransparentHeader ? {
				'--header-background': {
					from: '1',
					to: '0.1',
				},
			} :
			[],
			outside: (instance, percentage, props) => {
				console.log(props);
			}
		});
		customScroll.start();

	}

});

